# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [21.6.0](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/black/compare/v1.0.1...v21.6.0) (2021-06-14)

### [5.0.12](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/black/compare/v1.0.1...v5.0.12) (2021-06-14)

### [1.0.2](https://gitlab.com/ProfessorManhattan/code/compare/v1.0.1...v1.0.2) (2021-06-14)

### [1.0.2](https://gitlab.com/ProfessorManhattan/code/compare/v1.0.1...v1.0.2) (2021-06-14)

### [1.0.1](https://gitlab.com/ProfessorManhattan/code/compare/v0.0.24...v1.0.1) (2021-05-15)

### [0.0.24](https://gitlab.com/ProfessorManhattan/code/compare/v0.0.23...v0.0.24) (2021-05-13)

### 0.0.23 (2021-05-13)
